#include <stdio.h>
#include "student.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#define MAXCHARS 256

int main()
{
  Student *stud1;
  char buffer[MAXCHARS];
  char *end=NULL;
  unsigned id=0;
  errno=0;
  int a=1;
  int b=1;
  int c=1;


  while(b==1){
  while(a==1){
    printf("Enter the student id: ");
    if(fgets(buffer, MAXCHARS, stdin) == NULL){
    fprintf(stderr, "failed to read string, try again\n");
    a=1;
    }else{a=0;}
    }

    strtok(buffer, "\n");
    id = strtoul(buffer, &end, 10);
    if (errno !=0){
    fprintf(stderr, "Conversion error %s, try again\n", strerror(errno));
    b=1;
    }else if (*end){
      printf ("Warning : converted partially: %i, non-convertible part%s\n", id ,end);
      b=0;
    }
 }

 stud1=make_student(id);
 while(c==1){
 printf("Enter student name: ");
 if (fgets(buffer, MAXCHARS, stdin) == NULL){
   fprintf(stderr, "failed to read string, try again\n");
   c=1;    
   }else{c=0;}
   }
 strtok(buffer, "\n");
 set_student_name(stud1, buffer);


  make_student_active(stud1);
  if (is_student_active(stud1))
    {printf ("Student %u name is %s\n", stud1->id, stud1->name);
    }
  free_student(stud1);

  return EXIT_SUCCESS;
     
}
