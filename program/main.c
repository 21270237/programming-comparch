#include <stdio.h>
#include <stdbool.h>
#include <string.h>


int main()

{
	bool bExit = false;
	int select=1, max=0, i=1, price[max], amount[max], id[max], num, a=1, ch, sum=0,tot,u=0;
	float avg=1;
	FILE *fam;
	FILE *fpr;
	FILE *fid;

	/* here I wanted to add clrscr(); and after the while, getch();, but my compiler does not have conio.h */

	/* opens file in order to count how many items are in total */
	fam = fopen("amount.db", "r");
	while(fscanf(fam, "%d", &num) > 0)
		max++;
	printf("\n");
	/* prints how many items are in total */
	printf("You curently have %d items \n", max);
	fclose(fam);


	/* do-while loop, until bExit is true, so that the menu repeats */
	do
	{
		printf("\n");
		printf("What would you like to do ?:\n");
		printf("\n");
		printf("1) Add product \n");
		printf("2) Display all info of all products \n");
		printf("3) Find product by ID (shows amount and price) \n");
		printf("4) Find total amount of products\n");
		printf("5) Find the average price \n");
		printf("6) Find the total price for all items \n");
		printf("7) Exit \n");
		printf("\n");
		printf("Enter number for selection : ");
		scanf("%d", &select);
		printf("\n");
		/* the selector will decide on what case the program will go */

		switch (select)
		{
		/* case 1 will add another product (1 id, 1 amount, 1 price)  */
		case 1:
		{
			max++;
			/* opens id database, and then writes the new id on the last place */
			fid = fopen("id.db", "a");
			printf("Enter the id -> ");
			scanf("%d", &id[max]);
			fprintf(fid,"%d ", id[max]);
			fclose(fid);
			printf("%d", id[max]);

			/* opens amount database, and then writes the new amount on the last place */
			fam = fopen("amount.db", "a");
			printf("Enter the amount -> ");
			scanf("%d", &amount[max]);
			fprintf(fam,"%d ", amount[max]);
			fclose(fam);


			/* opens price database, and then writes the new price on the last place */
			fpr = fopen("price.db", "a");
			printf("Enter the price -> ");
			scanf("%d", &price[max]);
			fprintf(fpr,"%d ", price[max]);
			fclose(fpr);

			break;
		}

		/* case 2 will display a table of all the products and their amount and price */
		case 2:
		{




			/* opens id database, and read all the data in an array id[] */
			i=1;num=0;
			fid = fopen("id.db", "r");
			while(fscanf(fid, "%d", &num) > 0)
			{
				id[i] = num;
				i++;
			}
			fclose(fid);

			/* opens amount database, and read all the data in an array amount[] */
			i=1;num=0;
			fam = fopen("amount.db", "r");
			while(fscanf(fam, "%d", &num) > 0)
			{
				amount[i] = num;
				i++;
			}
			fclose(fam);

			/* opens price database, and read all the data in an array price[] */
			i=1;num=0;
			fpr = fopen("price.db", "r");
			while(fscanf(fpr, "%d", &num) > 0)
			{
				price[i] = num;
				i++;
			}


			/* print a table with the information */
			printf("	ID		AMOUNT		PRICE\n");
			printf("	-----------------------------------------\n");
			for(a=1;a<max+1;a++)
			{
				printf("%d:	|%d	|	%d	|	%d	|\n",a, id[a],amount[a],price[a]);
				printf("	-----------------------------------------\n");
			}
			break;
		}

		/* case 3 allows you to see al the information of a certain product ( found by id) */
		case 3:
		{
			/* just like in case 2, the databases are all writen in coresponding arrays */
			i=1;num=0;
			fid = fopen("id.db", "r");
			while(fscanf(fid, "%d", &num) > 0)
			{
				id[i] = num;
				i++;
			}
			fclose(fid);


			i=1;num=0;
			fpr = fopen("price.db", "r");
			while(fscanf(fpr, "%d", &num) > 0)
			{
				price[i] = num;
				i++;
			}
			fclose(fpr);


			i=1;num=0;
			fam = fopen("amount.db", "r");
			while(fscanf(fam, "%d", &num) > 0)
			{
				amount[i] = num;
				i++;
			}
			fclose(fam);

			/* the users enters an id, and if the id apears in the database, then all information about it will be shown */
			printf("Enter the ID you want information about -> ");
			scanf("%d", &ch);
			printf("\n");
			for(a=1;a<i;a++)
			{
				if(ch==id[a])
				{
					printf("		ID		AMOUNT		PRICE\n");
					printf("	-------------------------------------------------\n");
					printf("%d:	|	%d	|	%d	|	%d	|\n",a, id[a],amount[a],price[a]);
					printf("	-------------------------------------------------\n");
					u++;
				}
				if(u==0)
				{
					printf(" ID not found ! ");
				}
			}
			break;
		}


		/* case 4 will tell the sum of all amounts of the products */
		case 4:
		{

			/* the amount db is writen in an array */
			i=1;
			fam = fopen("amount.db", "r");
			while(fscanf(fam, "%d", &num) > 0)
			{
				amount[i] = num;
				i++;
			}
			fclose(fam);
			/* then a varible "tot", has each element of the array added to it */
			tot=0;
			for(a=1;a<i;a++)
				tot=tot+amount[a];
			printf("|--------------------------------------------------|\n");
			printf("| The total amount of all the products is -> %d <-|\n",tot);
			printf("|--------------------------------------------------|\n");

			break;
		}

		/* case 5 will show you what the average price of your products is */
		case 5:
		{
			i=1;
			/* the price db is writen in an array */
			fpr = fopen("price.db", "r");
			while(fscanf(fpr, "%d", &num) > 0)
			{
				price[i] = num;
				i++;
			}
			fclose(fpr);
			/* then a varible "sum", has each element of the array added to it */
			sum=0;
			for(a=1;a<i;a++)
				sum=sum+price[a];
			/* then we print the sum divided by the number of elements */
			printf("|-------------------------------|\n");
			printf("| The average price is -> %d <- |\n", sum/i);
			printf("|-------------------------------|\n");
			break;
		}

		/* case 6 will show you how much money you would receive if you sell all products instantly */
		case 6:
		{

			/* price db is writen in an array, and so it amount db */
			i=1;
			fpr = fopen("price.db", "r");
			while(fscanf(fpr, "%d", &num) > 0)
			{
				price[i] = num;
				i++;
			}
			fclose(fpr);

			i=1;
			fam = fopen("amount.db", "r");
			while(fscanf(fam, "%d", &num) > 0)
			{
				amount[i] = num;
				i++;
			}
			fclose(fam);


			/* the total price is the sum of the prices of each product times their amount */
			tot=0;
			for(a=1;a<i;a++)
				tot=tot + (price[a]*amount[a]);
			printf("|----------------------------------------------|\n");
			printf("|The total price of all products is -> %d <--|\n",tot);
			printf("|----------------------------------------------|\n");
			break;
		}

		/* case 7 , and anyother number exempt 1 to 6, will exit the program */
		case 7:
		{
			bExit= true;
			break;
		}

		default:
		{
			printf("Good Bye \n");
			bExit = true;
			break;
		}

		}
		/* the while from the do-while statement that allows us to repeat the menu until the user decides to exit */

	}while (!bExit);

	return 0;
}





