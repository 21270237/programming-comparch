#include <stdio.h>
#include <stdlib.h>

typedef struct
{
  unsigned id;
  int active;
}Student;

Student *make_student(unsigned id);
void free_student (Student *stud);
void make_student_active(Student *stud);
int is_student_active(Student *stud);

int main()
{
  Student *stud1;

  stud1 = make_student(1001);
  make_student_active(stud1);
  if (is_student_active(stud1))
    {printf ("student %d is active\n" , stud1->id);
    }
  free_student(stud1);
  return EXIT_SUCCESS;

}


Student *make_student(unsigned id)
{

Student *stud;

 if((stud=(Student *)malloc(sizeof(Student)))==NULL)
   {
     fprintf(stderr , "Failed to allocate Student structure!\n");
     exit(EXIT_FAILURE);
   }

   stud ->active = 0;
   stud ->id = id;
   return stud;

}

 void free_student(Student *stud)
  {
    free(stud);
  }

 void make_student_active(Student *stud)
 {
   stud->active =1;
 }

 int is_student_active(Student *stud)
 {
   return stud->active;
 }
