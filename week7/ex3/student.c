#include <stdio.h>
#include <stdlib.h>
#include "student.h"

Student *make_student(unsigned id)
{

Student *stud;

 if((stud=(Student *)malloc(sizeof(Student)))==NULL)
   {
     fprintf(stderr , "Failed to allocate Student structure!\n");
     exit(EXIT_FAILURE);
   }

   stud ->active = 0;
   stud ->id = id;
   return stud;

}

 void free_student(Student *stud)
  {
    free(stud);
  }

 void make_student_active(Student *stud)
 {
   stud->active =1;
 }

 int is_student_active(Student *stud)
 {
   return stud->active;
 }
