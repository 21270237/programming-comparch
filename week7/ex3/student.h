#include <stdio.h>
#include <stdlib.h>

typedef struct{
  unsigned id;
  int active;
}Student;

Student *make_student(unsigned id);
void free_student (Student *stud);
void make_student_active(Student *stud);
int is_student_active(Student *stud);
