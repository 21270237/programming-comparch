#include <stdio.h>
#include "student.h"
int main()
{
  Student *stud1;

  stud1 = make_student(1001);
  make_student_active(stud1);
  if (is_student_active(stud1))
    {printf ("student %d is active\n" , stud1->id);
    }
  free_student(stud1);
  return EXIT_SUCCESS;

}
