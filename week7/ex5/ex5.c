#include <stdio.h>
#include <stdlib.h>

typedef struct
{
  unsigned id;
  int active;
}Film;

Film *make_film(unsigned id);
void free_film (Film *fil);
void make_film_active(Film *fil);
int is_film_active(Film *fil);

int main()
{
  Film *fil1;

  fil1 = make_film(1001);
  make_film_active(fil1);
  if (is_film_active(fil1))
    {printf ("film %d is active\n" , fil1->id);
    }
  free_film(fil1);
  return EXIT_SUCCESS;

}


Film *make_film(unsigned id)
{

Film *fil;

 if((fil=(Film *)malloc(sizeof(Film)))==NULL)
   {
     fprintf(stderr , "Failed to allocate Film structure!\n");
     exit(EXIT_FAILURE);
   }

   fil ->active = 0;
   fil ->id = id;
   return fil;

}

 void free_film(Film *fil)
  {
    free(fil);
  }

 void make_film_active(Film *fil)
 {
   fil->active =1;
 }

 int is_film_active(Film *fil)
 {
   return fil->active;
 }
